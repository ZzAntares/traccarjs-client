# Traccar.Maintenance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**start** | **Number** |  | [optional] 
**period** | **Number** |  | [optional] 
**attributes** | **Object** |  | [optional] 
