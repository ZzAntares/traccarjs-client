# Traccar.Calendar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**data** | **String** | base64 encoded in iCalendar format | [optional] 
**attributes** | **Object** |  | [optional] 
