# Traccar.Driver

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**uniqueId** | **String** |  | [optional] 
**attributes** | **Object** |  | [optional] 
