# Traccar.Command

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**deviceId** | **Number** |  | [optional] 
**description** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**attributes** | **Object** |  | [optional] 
