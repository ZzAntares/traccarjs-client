# Traccar.DefaultApi

All URIs are relative to *http://demo.traccar.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attributesComputedGet**](DefaultApi.md#attributesComputedGet) | **GET** /attributes/computed | Fetch a list of Attributes
[**attributesComputedIdDelete**](DefaultApi.md#attributesComputedIdDelete) | **DELETE** /attributes/computed/{id} | Delete an Attribute
[**attributesComputedIdPut**](DefaultApi.md#attributesComputedIdPut) | **PUT** /attributes/computed/{id} | Update an Attribute
[**attributesComputedPost**](DefaultApi.md#attributesComputedPost) | **POST** /attributes/computed | Create an Attribute
[**calendarsGet**](DefaultApi.md#calendarsGet) | **GET** /calendars | Fetch a list of Calendars
[**calendarsIdDelete**](DefaultApi.md#calendarsIdDelete) | **DELETE** /calendars/{id} | Delete a Calendar
[**calendarsIdPut**](DefaultApi.md#calendarsIdPut) | **PUT** /calendars/{id} | Update a Calendar
[**calendarsPost**](DefaultApi.md#calendarsPost) | **POST** /calendars | Create a Calendar
[**commandsGet**](DefaultApi.md#commandsGet) | **GET** /commands | Fetch a list of Saved Commands
[**commandsIdDelete**](DefaultApi.md#commandsIdDelete) | **DELETE** /commands/{id} | Delete a Saved Command
[**commandsIdPut**](DefaultApi.md#commandsIdPut) | **PUT** /commands/{id} | Update a Saved Command
[**commandsPost**](DefaultApi.md#commandsPost) | **POST** /commands | Create a Saved Command
[**commandsSendGet**](DefaultApi.md#commandsSendGet) | **GET** /commands/send | Fetch a list of Saved Commands supported by Device at the moment
[**commandsSendPost**](DefaultApi.md#commandsSendPost) | **POST** /commands/send | Dispatch commands to device
[**commandsTypesGet**](DefaultApi.md#commandsTypesGet) | **GET** /commands/types | Fetch a list of available Commands for the Device or all possible Commands if Device ommited
[**devicesGet**](DefaultApi.md#devicesGet) | **GET** /devices | Fetch a list of Devices
[**devicesIdAccumulatorsPut**](DefaultApi.md#devicesIdAccumulatorsPut) | **PUT** /devices/{id}/accumulators | Update total distance and hours of the Device
[**devicesIdDelete**](DefaultApi.md#devicesIdDelete) | **DELETE** /devices/{id} | Delete a Device
[**devicesIdPut**](DefaultApi.md#devicesIdPut) | **PUT** /devices/{id} | Update a Device
[**devicesPost**](DefaultApi.md#devicesPost) | **POST** /devices | Create a Device
[**driversGet**](DefaultApi.md#driversGet) | **GET** /drivers | Fetch a list of Drivers
[**driversIdDelete**](DefaultApi.md#driversIdDelete) | **DELETE** /drivers/{id} | Delete a Driver
[**driversIdPut**](DefaultApi.md#driversIdPut) | **PUT** /drivers/{id} | Update a Driver
[**driversPost**](DefaultApi.md#driversPost) | **POST** /drivers | Create a Driver
[**eventsIdGet**](DefaultApi.md#eventsIdGet) | **GET** /events/{id} | 
[**geofencesGet**](DefaultApi.md#geofencesGet) | **GET** /geofences | Fetch a list of Geofences
[**geofencesIdDelete**](DefaultApi.md#geofencesIdDelete) | **DELETE** /geofences/{id} | Delete a Geofence
[**geofencesIdPut**](DefaultApi.md#geofencesIdPut) | **PUT** /geofences/{id} | Update a Geofence
[**geofencesPost**](DefaultApi.md#geofencesPost) | **POST** /geofences | Create a Geofence
[**groupsGet**](DefaultApi.md#groupsGet) | **GET** /groups | Fetch a list of Groups
[**groupsIdDelete**](DefaultApi.md#groupsIdDelete) | **DELETE** /groups/{id} | Delete a Group
[**groupsIdPut**](DefaultApi.md#groupsIdPut) | **PUT** /groups/{id} | Update a Group
[**groupsPost**](DefaultApi.md#groupsPost) | **POST** /groups | Create a Group
[**maintenanceGet**](DefaultApi.md#maintenanceGet) | **GET** /maintenance | Fetch a list of Maintenance
[**maintenanceIdDelete**](DefaultApi.md#maintenanceIdDelete) | **DELETE** /maintenance/{id} | Delete a Maintenance
[**maintenanceIdPut**](DefaultApi.md#maintenanceIdPut) | **PUT** /maintenance/{id} | Update a Maintenance
[**maintenancePost**](DefaultApi.md#maintenancePost) | **POST** /maintenance | Create a Maintenance
[**notificationsGet**](DefaultApi.md#notificationsGet) | **GET** /notifications | Fetch a list of Notifications
[**notificationsIdDelete**](DefaultApi.md#notificationsIdDelete) | **DELETE** /notifications/{id} | Delete a Notification
[**notificationsIdPut**](DefaultApi.md#notificationsIdPut) | **PUT** /notifications/{id} | Update a Notification
[**notificationsPost**](DefaultApi.md#notificationsPost) | **POST** /notifications | Create a Notification
[**notificationsTestPost**](DefaultApi.md#notificationsTestPost) | **POST** /notifications/test | Send test notification to current user via Email and SMS
[**notificationsTypesGet**](DefaultApi.md#notificationsTypesGet) | **GET** /notifications/types | Fetch a list of available Notification types
[**permissionsDelete**](DefaultApi.md#permissionsDelete) | **DELETE** /permissions | Unlink an Object from another Object
[**permissionsPost**](DefaultApi.md#permissionsPost) | **POST** /permissions | Link an Object to another Object
[**positionsGet**](DefaultApi.md#positionsGet) | **GET** /positions | Fetches a list of Positions
[**reportsEventsGet**](DefaultApi.md#reportsEventsGet) | **GET** /reports/events | Fetch a list of Events within the time period for the Devices or Groups
[**reportsRouteGet**](DefaultApi.md#reportsRouteGet) | **GET** /reports/route | Fetch a list of Positions within the time period for the Devices or Groups
[**reportsStopsGet**](DefaultApi.md#reportsStopsGet) | **GET** /reports/stops | Fetch a list of ReportStops within the time period for the Devices or Groups
[**reportsSummaryGet**](DefaultApi.md#reportsSummaryGet) | **GET** /reports/summary | Fetch a list of ReportSummary within the time period for the Devices or Groups
[**reportsTripsGet**](DefaultApi.md#reportsTripsGet) | **GET** /reports/trips | Fetch a list of ReportTrips within the time period for the Devices or Groups
[**serverGet**](DefaultApi.md#serverGet) | **GET** /server | Fetch Server information
[**serverPut**](DefaultApi.md#serverPut) | **PUT** /server | Update Server information
[**sessionDelete**](DefaultApi.md#sessionDelete) | **DELETE** /session | Close the Session
[**sessionGet**](DefaultApi.md#sessionGet) | **GET** /session | Fetch Session information
[**sessionPost**](DefaultApi.md#sessionPost) | **POST** /session | Create a new Session
[**statisticsGet**](DefaultApi.md#statisticsGet) | **GET** /statistics | Fetch server Statistics
[**usersGet**](DefaultApi.md#usersGet) | **GET** /users | Fetch a list of Users
[**usersIdDelete**](DefaultApi.md#usersIdDelete) | **DELETE** /users/{id} | Delete a User
[**usersIdPut**](DefaultApi.md#usersIdPut) | **PUT** /users/{id} | Update a User
[**usersPost**](DefaultApi.md#usersPost) | **POST** /users | Create a User

<a name="attributesComputedGet"></a>
# **attributesComputedGet**
> [Attribute] attributesComputedGet(opts)

Fetch a list of Attributes

Without params, it returns a list of Attributes the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.attributesComputedGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Attribute]**](Attribute.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="attributesComputedIdDelete"></a>
# **attributesComputedIdDelete**
> attributesComputedIdDelete(id)

Delete an Attribute

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.attributesComputedIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="attributesComputedIdPut"></a>
# **attributesComputedIdPut**
> Attribute attributesComputedIdPut(bodyid)

Update an Attribute

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Attribute(); // Attribute | 
let id = 56; // Number | 

apiInstance.attributesComputedIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Attribute**](Attribute.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Attribute**](Attribute.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="attributesComputedPost"></a>
# **attributesComputedPost**
> Attribute attributesComputedPost(body)

Create an Attribute

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Attribute(); // Attribute | 

apiInstance.attributesComputedPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Attribute**](Attribute.md)|  | 

### Return type

[**Attribute**](Attribute.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="calendarsGet"></a>
# **calendarsGet**
> [Calendar] calendarsGet(opts)

Fetch a list of Calendars

Without params, it returns a list of Calendars the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56 // Number | Standard users can use this only with their own _userId_
};
apiInstance.calendarsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 

### Return type

[**[Calendar]**](Calendar.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="calendarsIdDelete"></a>
# **calendarsIdDelete**
> calendarsIdDelete(id)

Delete a Calendar

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.calendarsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="calendarsIdPut"></a>
# **calendarsIdPut**
> Calendar calendarsIdPut(bodyid)

Update a Calendar

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Calendar(); // Calendar | 
let id = 56; // Number | 

apiInstance.calendarsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Calendar**](Calendar.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Calendar**](Calendar.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="calendarsPost"></a>
# **calendarsPost**
> Calendar calendarsPost(body)

Create a Calendar

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Calendar(); // Calendar | 

apiInstance.calendarsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Calendar**](Calendar.md)|  | 

### Return type

[**Calendar**](Calendar.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="commandsGet"></a>
# **commandsGet**
> [Command] commandsGet(opts)

Fetch a list of Saved Commands

Without params, it returns a list of Drivers the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.commandsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Command]**](Command.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="commandsIdDelete"></a>
# **commandsIdDelete**
> commandsIdDelete(id)

Delete a Saved Command

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.commandsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="commandsIdPut"></a>
# **commandsIdPut**
> Command commandsIdPut(bodyid)

Update a Saved Command

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Command(); // Command | 
let id = 56; // Number | 

apiInstance.commandsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Command**](Command.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Command**](Command.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="commandsPost"></a>
# **commandsPost**
> Command commandsPost(body)

Create a Saved Command

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Command(); // Command | 

apiInstance.commandsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Command**](Command.md)|  | 

### Return type

[**Command**](Command.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="commandsSendGet"></a>
# **commandsSendGet**
> [Command] commandsSendGet(opts)

Fetch a list of Saved Commands supported by Device at the moment

Return a list of saved commands linked to Device and its groups, filtered by current Device protocol support

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'deviceId': 56 // Number | Standard users can use this only with _deviceId_s, they have access to
};
apiInstance.commandsSendGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 

### Return type

[**[Command]**](Command.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="commandsSendPost"></a>
# **commandsSendPost**
> Command commandsSendPost(body)

Dispatch commands to device

Dispatch a new command or Saved Command if _body.id_ set

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Command(); // Command | 

apiInstance.commandsSendPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Command**](Command.md)|  | 

### Return type

[**Command**](Command.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="commandsTypesGet"></a>
# **commandsTypesGet**
> [CommandType] commandsTypesGet(opts)

Fetch a list of available Commands for the Device or all possible Commands if Device ommited

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'deviceId': 56, // Number | Internal device identifier. Only works if device has already reported some locations
  'protocol': "protocol_example", // String | Protocol name. Can be used instead of device id
  'textChannel': true // Boolean | When `true` return SMS commands. If not specified or `false` return data commands
};
apiInstance.commandsTypesGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **Number**| Internal device identifier. Only works if device has already reported some locations | [optional] 
 **protocol** | **String**| Protocol name. Can be used instead of device id | [optional] 
 **textChannel** | **Boolean**| When &#x60;true&#x60; return SMS commands. If not specified or &#x60;false&#x60; return data commands | [optional] 

### Return type

[**[CommandType]**](CommandType.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="devicesGet"></a>
# **devicesGet**
> [Device] devicesGet(opts)

Fetch a list of Devices

Without any params, returns a list of the user&#x27;s devices

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'id': 56, // Number | To fetch one or more devices. Multiple params can be passed like `id=31&id=42`
  'uniqueId': "uniqueId_example" // String | To fetch one or more devices. Multiple params can be passed like `uniqueId=333331&uniqieId=44442`
};
apiInstance.devicesGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **id** | **Number**| To fetch one or more devices. Multiple params can be passed like &#x60;id&#x3D;31&amp;id&#x3D;42&#x60; | [optional] 
 **uniqueId** | **String**| To fetch one or more devices. Multiple params can be passed like &#x60;uniqueId&#x3D;333331&amp;uniqieId&#x3D;44442&#x60; | [optional] 

### Return type

[**[Device]**](Device.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="devicesIdAccumulatorsPut"></a>
# **devicesIdAccumulatorsPut**
> devicesIdAccumulatorsPut(bodyid)

Update total distance and hours of the Device

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.DeviceAccumulators(); // DeviceAccumulators | 
let id = 56; // Number | 

apiInstance.devicesIdAccumulatorsPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeviceAccumulators**](DeviceAccumulators.md)|  | 
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="devicesIdDelete"></a>
# **devicesIdDelete**
> devicesIdDelete(id)

Delete a Device

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.devicesIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="devicesIdPut"></a>
# **devicesIdPut**
> Device devicesIdPut(bodyid)

Update a Device

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Device(); // Device | 
let id = 56; // Number | 

apiInstance.devicesIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Device**](Device.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Device**](Device.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="devicesPost"></a>
# **devicesPost**
> Device devicesPost(body)

Create a Device

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Device(); // Device | 

apiInstance.devicesPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Device**](Device.md)|  | 

### Return type

[**Device**](Device.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="driversGet"></a>
# **driversGet**
> [Driver] driversGet(opts)

Fetch a list of Drivers

Without params, it returns a list of Drivers the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.driversGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Driver]**](Driver.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="driversIdDelete"></a>
# **driversIdDelete**
> driversIdDelete(id)

Delete a Driver

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.driversIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="driversIdPut"></a>
# **driversIdPut**
> Driver driversIdPut(bodyid)

Update a Driver

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Driver(); // Driver | 
let id = 56; // Number | 

apiInstance.driversIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Driver**](Driver.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Driver**](Driver.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="driversPost"></a>
# **driversPost**
> Driver driversPost(body)

Create a Driver

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Driver(); // Driver | 

apiInstance.driversPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Driver**](Driver.md)|  | 

### Return type

[**Driver**](Driver.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsIdGet"></a>
# **eventsIdGet**
> Event eventsIdGet(id)



### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.eventsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**Event**](Event.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="geofencesGet"></a>
# **geofencesGet**
> [Geofence] geofencesGet(opts)

Fetch a list of Geofences

Without params, it returns a list of Geofences the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.geofencesGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Geofence]**](Geofence.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="geofencesIdDelete"></a>
# **geofencesIdDelete**
> geofencesIdDelete(id)

Delete a Geofence

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.geofencesIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="geofencesIdPut"></a>
# **geofencesIdPut**
> Geofence geofencesIdPut(bodyid)

Update a Geofence

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Geofence(); // Geofence | 
let id = 56; // Number | 

apiInstance.geofencesIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Geofence**](Geofence.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Geofence**](Geofence.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="geofencesPost"></a>
# **geofencesPost**
> Geofence geofencesPost(body)

Create a Geofence

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Geofence(); // Geofence | 

apiInstance.geofencesPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Geofence**](Geofence.md)|  | 

### Return type

[**Geofence**](Geofence.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="groupsGet"></a>
# **groupsGet**
> [Group] groupsGet(opts)

Fetch a list of Groups

Without any params, returns a list of the Groups the user belongs to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56 // Number | Standard users can use this only with their own _userId_
};
apiInstance.groupsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 

### Return type

[**[Group]**](Group.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="groupsIdDelete"></a>
# **groupsIdDelete**
> groupsIdDelete(id)

Delete a Group

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.groupsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="groupsIdPut"></a>
# **groupsIdPut**
> Group groupsIdPut(bodyid)

Update a Group

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Group(); // Group | 
let id = 56; // Number | 

apiInstance.groupsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Group**](Group.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Group**](Group.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="groupsPost"></a>
# **groupsPost**
> Group groupsPost(body)

Create a Group

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Group(); // Group | 

apiInstance.groupsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Group**](Group.md)|  | 

### Return type

[**Group**](Group.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="maintenanceGet"></a>
# **maintenanceGet**
> [Maintenance] maintenanceGet(opts)

Fetch a list of Maintenance

Without params, it returns a list of Maintenance the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.maintenanceGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Maintenance]**](Maintenance.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="maintenanceIdDelete"></a>
# **maintenanceIdDelete**
> maintenanceIdDelete(id)

Delete a Maintenance

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.maintenanceIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="maintenanceIdPut"></a>
# **maintenanceIdPut**
> Maintenance maintenanceIdPut(bodyid)

Update a Maintenance

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Maintenance(); // Maintenance | 
let id = 56; // Number | 

apiInstance.maintenanceIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Maintenance**](Maintenance.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Maintenance**](Maintenance.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="maintenancePost"></a>
# **maintenancePost**
> Maintenance maintenancePost(body)

Create a Maintenance

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Maintenance(); // Maintenance | 

apiInstance.maintenancePost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Maintenance**](Maintenance.md)|  | 

### Return type

[**Maintenance**](Maintenance.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="notificationsGet"></a>
# **notificationsGet**
> [Notification] notificationsGet(opts)

Fetch a list of Notifications

Without params, it returns a list of Notifications the user has access to

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'all': true, // Boolean | Can only be used by admins or managers to fetch all entities
  'userId': 56, // Number | Standard users can use this only with their own _userId_
  'deviceId': 56, // Number | Standard users can use this only with _deviceId_s, they have access to
  'groupId': 56, // Number | Standard users can use this only with _groupId_s, they have access to
  'refresh': true // Boolean | 
};
apiInstance.notificationsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **all** | **Boolean**| Can only be used by admins or managers to fetch all entities | [optional] 
 **userId** | **Number**| Standard users can use this only with their own _userId_ | [optional] 
 **deviceId** | **Number**| Standard users can use this only with _deviceId_s, they have access to | [optional] 
 **groupId** | **Number**| Standard users can use this only with _groupId_s, they have access to | [optional] 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**[Notification]**](Notification.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="notificationsIdDelete"></a>
# **notificationsIdDelete**
> notificationsIdDelete(id)

Delete a Notification

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.notificationsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="notificationsIdPut"></a>
# **notificationsIdPut**
> Notification notificationsIdPut(bodyid)

Update a Notification

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Notification(); // Notification | 
let id = 56; // Number | 

apiInstance.notificationsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Notification**](Notification.md)|  | 
 **id** | **Number**|  | 

### Return type

[**Notification**](Notification.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="notificationsPost"></a>
# **notificationsPost**
> Notification notificationsPost(body)

Create a Notification

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Notification(); // Notification | 

apiInstance.notificationsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Notification**](Notification.md)|  | 

### Return type

[**Notification**](Notification.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="notificationsTestPost"></a>
# **notificationsTestPost**
> notificationsTestPost()

Send test notification to current user via Email and SMS

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
apiInstance.notificationsTestPost((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="notificationsTypesGet"></a>
# **notificationsTypesGet**
> [NotificationType] notificationsTypesGet()

Fetch a list of available Notification types

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
apiInstance.notificationsTypesGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[NotificationType]**](NotificationType.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="permissionsDelete"></a>
# **permissionsDelete**
> permissionsDelete(body)

Unlink an Object from another Object

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Permission(); // Permission | 

apiInstance.permissionsDelete(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Permission**](Permission.md)|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="permissionsPost"></a>
# **permissionsPost**
> Permission permissionsPost(body)

Link an Object to another Object

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Permission(); // Permission | 

apiInstance.permissionsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Permission**](Permission.md)|  | 

### Return type

[**Permission**](Permission.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="positionsGet"></a>
# **positionsGet**
> [Position] positionsGet(opts)

Fetches a list of Positions

Without any params, it returns a list of last known positions for all the user&#x27;s Devices. _from_ and _to_ fields are not required with _id_

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'deviceId': 56, // Number | _deviceId_ is optional, but requires the _from_ and _to_ parameters when used
  'from': new Date("2013-10-20T19:20:30+01:00"), // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
  'to': new Date("2013-10-20T19:20:30+01:00"), // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
  'id': 56 // Number | To fetch one or more positions. Multiple params can be passed like `id=31&id=42`
};
apiInstance.positionsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **Number**| _deviceId_ is optional, but requires the _from_ and _to_ parameters when used | [optional] 
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | [optional] 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | [optional] 
 **id** | **Number**| To fetch one or more positions. Multiple params can be passed like &#x60;id&#x3D;31&amp;id&#x3D;42&#x60; | [optional] 

### Return type

[**[Position]**](Position.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv, application/gpx+xml

<a name="reportsEventsGet"></a>
# **reportsEventsGet**
> [Event] reportsEventsGet(from, to, opts)

Fetch a list of Events within the time period for the Devices or Groups

At least one _deviceId_ or one _groupId_ must be passed

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let opts = { 
  'deviceId': [3.4], // [Number] | 
  'groupId': [3.4], // [Number] | 
  'type': ["type_example"] // [String] | % can be used to return events of all types
};
apiInstance.reportsEventsGet(from, to, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **deviceId** | [**[Number]**](Number.md)|  | [optional] 
 **groupId** | [**[Number]**](Number.md)|  | [optional] 
 **type** | [**[String]**](String.md)| % can be used to return events of all types | [optional] 

### Return type

[**[Event]**](Event.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

<a name="reportsRouteGet"></a>
# **reportsRouteGet**
> [Position] reportsRouteGet(from, to, opts)

Fetch a list of Positions within the time period for the Devices or Groups

At least one _deviceId_ or one _groupId_ must be passed

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let opts = { 
  'deviceId': [3.4], // [Number] | 
  'groupId': [3.4] // [Number] | 
};
apiInstance.reportsRouteGet(from, to, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **deviceId** | [**[Number]**](Number.md)|  | [optional] 
 **groupId** | [**[Number]**](Number.md)|  | [optional] 

### Return type

[**[Position]**](Position.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

<a name="reportsStopsGet"></a>
# **reportsStopsGet**
> [ReportStops] reportsStopsGet(from, to, opts)

Fetch a list of ReportStops within the time period for the Devices or Groups

At least one _deviceId_ or one _groupId_ must be passed

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let opts = { 
  'deviceId': [3.4], // [Number] | 
  'groupId': [3.4] // [Number] | 
};
apiInstance.reportsStopsGet(from, to, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **deviceId** | [**[Number]**](Number.md)|  | [optional] 
 **groupId** | [**[Number]**](Number.md)|  | [optional] 

### Return type

[**[ReportStops]**](ReportStops.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

<a name="reportsSummaryGet"></a>
# **reportsSummaryGet**
> [ReportSummary] reportsSummaryGet(from, to, opts)

Fetch a list of ReportSummary within the time period for the Devices or Groups

At least one _deviceId_ or one _groupId_ must be passed

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let opts = { 
  'deviceId': [3.4], // [Number] | 
  'groupId': [3.4] // [Number] | 
};
apiInstance.reportsSummaryGet(from, to, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **deviceId** | [**[Number]**](Number.md)|  | [optional] 
 **groupId** | [**[Number]**](Number.md)|  | [optional] 

### Return type

[**[ReportSummary]**](ReportSummary.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

<a name="reportsTripsGet"></a>
# **reportsTripsGet**
> [ReportTrips] reportsTripsGet(from, to, opts)

Fetch a list of ReportTrips within the time period for the Devices or Groups

At least one _deviceId_ or one _groupId_ must be passed

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let opts = { 
  'deviceId': [3.4], // [Number] | 
  'groupId': [3.4] // [Number] | 
};
apiInstance.reportsTripsGet(from, to, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **deviceId** | [**[Number]**](Number.md)|  | [optional] 
 **groupId** | [**[Number]**](Number.md)|  | [optional] 

### Return type

[**[ReportTrips]**](ReportTrips.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

<a name="serverGet"></a>
# **serverGet**
> Server serverGet()

Fetch Server information

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
apiInstance.serverGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Server**](Server.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serverPut"></a>
# **serverPut**
> Server serverPut(body)

Update Server information

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.Server(); // Server | 

apiInstance.serverPut(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Server**](Server.md)|  | 

### Return type

[**Server**](Server.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sessionDelete"></a>
# **sessionDelete**
> sessionDelete()

Close the Session

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
apiInstance.sessionDelete((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="sessionGet"></a>
# **sessionGet**
> User sessionGet(opts)

Fetch Session information

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'token': "token_example" // String | 
};
apiInstance.sessionGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="sessionPost"></a>
# **sessionPost**
> User sessionPost(emailpassword)

Create a new Session

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let email = "email_example"; // String | 
let password = "password_example"; // String | 

apiInstance.sessionPost(emailpassword, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**|  | 
 **password** | [**String**](.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="statisticsGet"></a>
# **statisticsGet**
> [Statistics] statisticsGet(from, to)

Fetch server Statistics

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let from = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
let to = new Date("2013-10-20T19:20:30+01:00"); // Date | in IS0 8601 format. eg. `1963-11-22T18:30:00Z`

apiInstance.statisticsGet(from, to, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 
 **to** | **Date**| in IS0 8601 format. eg. &#x60;1963-11-22T18:30:00Z&#x60; | 

### Return type

[**[Statistics]**](Statistics.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersGet"></a>
# **usersGet**
> [User] usersGet(opts)

Fetch a list of Users

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let opts = { 
  'userId': "userId_example" // String | Can only be used by admin or manager users
};
apiInstance.usersGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| Can only be used by admin or manager users | [optional] 

### Return type

[**[User]**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdDelete"></a>
# **usersIdDelete**
> usersIdDelete(id)

Delete a User

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let id = 56; // Number | 

apiInstance.usersIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdPut"></a>
# **usersIdPut**
> User usersIdPut(bodyid)

Update a User

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.User(); // User | 
let id = 56; // Number | 

apiInstance.usersIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)|  | 
 **id** | **Number**|  | 

### Return type

[**User**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersPost"></a>
# **usersPost**
> User usersPost(body)

Create a User

### Example
```javascript
import Traccar from 'traccar';
let defaultClient = Traccar.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Traccar.DefaultApi();
let body = new Traccar.User(); // User | 

apiInstance.usersPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

