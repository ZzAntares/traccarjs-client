/**
 * traccar
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 4.8
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The Event model module.
* @module model/Event
* @version 4.8
*/
export default class Event {
    /**
    * Constructs a new <code>Event</code>.
    * @alias module:model/Event
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>Event</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/Event} obj Optional instance to populate.
    * @return {module:model/Event} The populated <code>Event</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Event();
                        
            
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
            if (data.hasOwnProperty('serverTime')) {
                obj['serverTime'] = ApiClient.convertToType(data['serverTime'], 'Date');
            }
            if (data.hasOwnProperty('deviceId')) {
                obj['deviceId'] = ApiClient.convertToType(data['deviceId'], 'Number');
            }
            if (data.hasOwnProperty('positionId')) {
                obj['positionId'] = ApiClient.convertToType(data['positionId'], 'Number');
            }
            if (data.hasOwnProperty('geofenceId')) {
                obj['geofenceId'] = ApiClient.convertToType(data['geofenceId'], 'Number');
            }
            if (data.hasOwnProperty('maintenanceId')) {
                obj['maintenanceId'] = ApiClient.convertToType(data['maintenanceId'], 'Number');
            }
            if (data.hasOwnProperty('attributes')) {
                obj['attributes'] = ApiClient.convertToType(data['attributes'], Object);
            }
        }
        return obj;
    }

    /**
    * @member {Number} id
    */
    'id' = undefined;
    /**
    * @member {String} type
    */
    'type' = undefined;
    /**
    * in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
    * @member {Date} serverTime
    */
    'serverTime' = undefined;
    /**
    * @member {Number} deviceId
    */
    'deviceId' = undefined;
    /**
    * @member {Number} positionId
    */
    'positionId' = undefined;
    /**
    * @member {Number} geofenceId
    */
    'geofenceId' = undefined;
    /**
    * @member {Number} maintenanceId
    */
    'maintenanceId' = undefined;
    /**
    * @member {Object} attributes
    */
    'attributes' = undefined;




}
