/**
 * traccar
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 4.8
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The Statistics model module.
* @module model/Statistics
* @version 4.8
*/
export default class Statistics {
    /**
    * Constructs a new <code>Statistics</code>.
    * @alias module:model/Statistics
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>Statistics</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/Statistics} obj Optional instance to populate.
    * @return {module:model/Statistics} The populated <code>Statistics</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Statistics();
                        
            
            if (data.hasOwnProperty('captureTime')) {
                obj['captureTime'] = ApiClient.convertToType(data['captureTime'], 'Date');
            }
            if (data.hasOwnProperty('activeUsers')) {
                obj['activeUsers'] = ApiClient.convertToType(data['activeUsers'], 'Number');
            }
            if (data.hasOwnProperty('activeDevices')) {
                obj['activeDevices'] = ApiClient.convertToType(data['activeDevices'], 'Number');
            }
            if (data.hasOwnProperty('requests')) {
                obj['requests'] = ApiClient.convertToType(data['requests'], 'Number');
            }
            if (data.hasOwnProperty('messagesReceived')) {
                obj['messagesReceived'] = ApiClient.convertToType(data['messagesReceived'], 'Number');
            }
            if (data.hasOwnProperty('messagesStored')) {
                obj['messagesStored'] = ApiClient.convertToType(data['messagesStored'], 'Number');
            }
        }
        return obj;
    }

    /**
    * in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
    * @member {Date} captureTime
    */
    'captureTime' = undefined;
    /**
    * @member {Number} activeUsers
    */
    'activeUsers' = undefined;
    /**
    * @member {Number} activeDevices
    */
    'activeDevices' = undefined;
    /**
    * @member {Number} requests
    */
    'requests' = undefined;
    /**
    * @member {Number} messagesReceived
    */
    'messagesReceived' = undefined;
    /**
    * @member {Number} messagesStored
    */
    'messagesStored' = undefined;




}
