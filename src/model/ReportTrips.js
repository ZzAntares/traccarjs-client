/**
 * traccar
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 4.8
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The ReportTrips model module.
* @module model/ReportTrips
* @version 4.8
*/
export default class ReportTrips {
    /**
    * Constructs a new <code>ReportTrips</code>.
    * @alias module:model/ReportTrips
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>ReportTrips</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ReportTrips} obj Optional instance to populate.
    * @return {module:model/ReportTrips} The populated <code>ReportTrips</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReportTrips();
                        
            
            if (data.hasOwnProperty('deviceId')) {
                obj['deviceId'] = ApiClient.convertToType(data['deviceId'], 'Number');
            }
            if (data.hasOwnProperty('deviceName')) {
                obj['deviceName'] = ApiClient.convertToType(data['deviceName'], 'String');
            }
            if (data.hasOwnProperty('maxSpeed')) {
                obj['maxSpeed'] = ApiClient.convertToType(data['maxSpeed'], 'Number');
            }
            if (data.hasOwnProperty('averageSpeed')) {
                obj['averageSpeed'] = ApiClient.convertToType(data['averageSpeed'], 'Number');
            }
            if (data.hasOwnProperty('distance')) {
                obj['distance'] = ApiClient.convertToType(data['distance'], 'Number');
            }
            if (data.hasOwnProperty('spentFuel')) {
                obj['spentFuel'] = ApiClient.convertToType(data['spentFuel'], 'Number');
            }
            if (data.hasOwnProperty('duration')) {
                obj['duration'] = ApiClient.convertToType(data['duration'], 'Number');
            }
            if (data.hasOwnProperty('startTime')) {
                obj['startTime'] = ApiClient.convertToType(data['startTime'], 'Date');
            }
            if (data.hasOwnProperty('startAddress')) {
                obj['startAddress'] = ApiClient.convertToType(data['startAddress'], 'String');
            }
            if (data.hasOwnProperty('startLat')) {
                obj['startLat'] = ApiClient.convertToType(data['startLat'], 'Number');
            }
            if (data.hasOwnProperty('startLon')) {
                obj['startLon'] = ApiClient.convertToType(data['startLon'], 'Number');
            }
            if (data.hasOwnProperty('endTime')) {
                obj['endTime'] = ApiClient.convertToType(data['endTime'], 'Date');
            }
            if (data.hasOwnProperty('endAddress')) {
                obj['endAddress'] = ApiClient.convertToType(data['endAddress'], 'String');
            }
            if (data.hasOwnProperty('endLat')) {
                obj['endLat'] = ApiClient.convertToType(data['endLat'], 'Number');
            }
            if (data.hasOwnProperty('endLon')) {
                obj['endLon'] = ApiClient.convertToType(data['endLon'], 'Number');
            }
            if (data.hasOwnProperty('driverUniqueId')) {
                obj['driverUniqueId'] = ApiClient.convertToType(data['driverUniqueId'], 'Number');
            }
            if (data.hasOwnProperty('driverName')) {
                obj['driverName'] = ApiClient.convertToType(data['driverName'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {Number} deviceId
    */
    'deviceId' = undefined;
    /**
    * @member {String} deviceName
    */
    'deviceName' = undefined;
    /**
    * in knots
    * @member {Number} maxSpeed
    */
    'maxSpeed' = undefined;
    /**
    * in knots
    * @member {Number} averageSpeed
    */
    'averageSpeed' = undefined;
    /**
    * in meters
    * @member {Number} distance
    */
    'distance' = undefined;
    /**
    * in liters
    * @member {Number} spentFuel
    */
    'spentFuel' = undefined;
    /**
    * @member {Number} duration
    */
    'duration' = undefined;
    /**
    * in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
    * @member {Date} startTime
    */
    'startTime' = undefined;
    /**
    * @member {String} startAddress
    */
    'startAddress' = undefined;
    /**
    * @member {Number} startLat
    */
    'startLat' = undefined;
    /**
    * @member {Number} startLon
    */
    'startLon' = undefined;
    /**
    * in IS0 8601 format. eg. `1963-11-22T18:30:00Z`
    * @member {Date} endTime
    */
    'endTime' = undefined;
    /**
    * @member {String} endAddress
    */
    'endAddress' = undefined;
    /**
    * @member {Number} endLat
    */
    'endLat' = undefined;
    /**
    * @member {Number} endLon
    */
    'endLon' = undefined;
    /**
    * @member {Number} driverUniqueId
    */
    'driverUniqueId' = undefined;
    /**
    * @member {String} driverName
    */
    'driverName' = undefined;




}
